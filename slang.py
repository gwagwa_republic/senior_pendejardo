ABBR_DICT = {
    'wtf': {
        'es': 'watefoc',
        'en': 'what the fuck'
    },
    'mf': {
        'es': 'moderfoker',
        'en': 'motherfucker'
    },
    'wth': {
        'es': 'watejel',
        'en': 'what the hell'
    },
    'smh': {
        'es': 'shekinmajed',
        'en': 'shaking my head'
    },
    'omg': {
        'es': 'omaigad',
        'en': 'oh my god'
    },
    'ok': {
        'es': 'okey',
        'en': 'okay'
    },
    'tbh': {
        'es': 'tu bi onest',
        'en': 'to be honest'
    },
    'brb': {
        'en': 'be right back',
        'es': 'bi rait bak'
    },
    'ftw': {
        'en': 'for the win',
        'es': 'for de win'
    },
    'nsfw': {
        'en': 'not safe for work',
        'es': 'not seif for work'
    },
    'omw': {
        'en': 'on my way',
        'es': 'on mai wei'
    },
    'hbu': {
        'en': 'how about you',
        'es': 'jai abaut yu'
    },
    'idk': {
        'en': 'i don\'t know',
        'es': 'ai don nou'
    },
    'irl': {
        'en': 'in real life',
        'es': 'in rial laif'
    },
    'kms': {
        'en': 'kill myself',
        'es': 'kil mai self'
    },
    'ngk': {
        'en': 'not gonna lie',
        'es': 'not gona lai'
    },
    'nvm': {
        'en': 'nevermind',
        'es': 'never maind'
    },
    'idc': {
        'en': 'i don\'t care',
        'es': 'ai dont ker'
    },
    'stfu': {
        'en': 'shut the fuck up',
        'es': 'shot de foc up'
    }
}
