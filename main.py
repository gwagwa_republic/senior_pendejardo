import os
import re
from random import randint

import discord
from discord import FFmpegPCMAudio
from dotenv import dotenv_values
from emoji import emojize
from gtts import gTTS

from slang import ABBR_DICT

config = dotenv_values('./.env')

intents = discord.Intents.all()

client = discord.Client(command_prefix="'", intents=intents)

ENGLISH_BAD_WORDS = []
SPANISH_BAD_WORDS = []
DEBUG = True
VOICE = None


@client.event
async def on_ready():
    print('We have logged in as {0.user}'.format(client))


@client.event
async def on_message(message):
    global VOICE

    if message.author == client.user and message.content.startswith != "'":
        return
    else:
        # DEBUG
        if DEBUG:
            print(message.content, len(message.content.split(' ')))

        # SALUDO
        if message.content[1:] == 'hi':
            await message.channel.send('HOLA!')
            await message.channel.send(file=discord.File('HOLA.png'))

        if message.content[1:] == 'dado':
            die_face = [':one:', ':two:', ':three:', ':four:', ':five:', ':six:']
            await message.channel.send(emojize(':game_die:') + '  :  ' + emojize(die_face[randint(0, 5)]))

        if message.content[1:] == 'lanzamoneda':
            coin_face = ['ÁGUILA', 'SOL']
            await message.channel.send(emojize(':coin:') + '  :  ' + coin_face[randint(0, 1)])

        if message.content[1:] == 'fedo':
            await message.channel.send('somos generales')
            await message.channel.send(file=discord.File('los generales.jpg'))

        # CANAL DE VOZ
        if message.content[1:] == 'entrale':
            await play(message.author.voice.channel, 'YANOCABEMOS.wav')

        if message.content[1:].startswith('di') or message.content[1:].startswith('say'):
            phrase = str(' '.join(message.content.split(' ')[1:])).lower()
            if message.content[1:].startswith('di'):
                lang = 'es'
                tld = 'com.mx'
            if message.content[1:].startswith('say'):
                lang = 'en'
                tld = 'com'
            for abbr in ABBR_DICT.keys():
                if abbr in phrase:
                    abbr_regex = r'\s*' + re.escape(abbr) + r'\s*'
                    phrase = re.sub(abbr_regex, ' ' + ABBR_DICT[abbr][lang] + ' ', phrase)
                    print(phrase)
            await channel_connnect(message.author.voice.channel)
            await tts_talk(phrase=phrase, audio_file='tts.mp3', lang=lang, tld=tld)
            await channel_disconnect()

        if message.content[1:] == 'yeeHa':
            await play(message.author.voice.channel, 'yeeHa.wav')

        if message.content[1:] == 'awebo':
            await play(message.author.voice.channel, 'awebo.mp3')

        if message.content[1:] == 'america':
            await play(message.author.voice.channel, 'america.mp3')

        if message.content[1:] == 'salgase':
            await play(message.author.voice.channel, 'doit.mp3'),
            await channel_disconnect()

        if message.content[1:] == 'doit':
            await play(message.author.voice.channel, 'doit.mp3'),

        if message.content[1:] == 'grosero':
            await channel_connnect(message.author.voice.channel)
            phrase = choose_bad_word('./bad_words/')
            await tts_talk(phrase=phrase[0], audio_file='tts.mp3', lang=phrase[1], tld=phrase[2]),
            await channel_disconnect()


async def channel_connnect(channel):
    global VOICE
    if VOICE == None:
        VOICE = await channel.connect()


async def channel_disconnect():
    global VOICE
    VOICE = None
    for channel in client.voice_clients:
        await channel.disconnect()


async def play(channel, audio_file):
    await channel_connnect(channel)
    source = FFmpegPCMAudio(audio_file)
    VOICE.play(source)


async def tts_talk(phrase, audio_file, lang, tld):
    tts = gTTS(text=phrase, lang=lang, tld=tld)
    tts.save(audio_file)
    source = FFmpegPCMAudio(audio_file)
    await VOICE.play(source)


def load_bad_words(bad_words_dir):
    for file in os.listdir(bad_words_dir):
        with open(bad_words_dir + file) as bad_word_list:
            if file[:2] == 'es':
                print(bad_words_dir + file)
                global SPANISH_BAD_WORDS
                SPANISH_BAD_WORDS += bad_word_list.read().split(',')
            if file[:2] == 'en':
                global ENGLISH_BAD_WORDS
                ENGLISH_BAD_WORDS += bad_word_list.read().split(',')


def choose_bad_word(bad_words_path):
    load_bad_words(bad_words_path)
    if randint(0, 1) == 0:
        lang = 'es'
        tld = 'com.mx'
        BAD_WORDS = SPANISH_BAD_WORDS
    else:
        lang = 'en'
        tld = 'com'
        BAD_WORDS = ENGLISH_BAD_WORDS
    bad_word_index = randint(0, len(BAD_WORDS) - 1)
    return (BAD_WORDS[bad_word_index].strip().lower(), lang, tld)


client.run(config['DISCORD_API'])
